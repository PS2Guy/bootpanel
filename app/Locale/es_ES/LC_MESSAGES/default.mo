��    t      �  �   \      �	     �	  #   �	     
     

     
  	   
      
  !   )
  !   K
     m
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
  	   �
     �
            	     
   (     3  	   8     B  	   I     S     [     c     h     p     x     }     �     �     �     �     �  '   �     �     �                    %  	   ,     6     :  	   G     Q     Z  	   _     i     n     �  %   �     �     �     �     �     �     �     �                    %     4     =     K     S     e     t     |     �     �     �     �     �     �     �  	   �     �     �     �       	        #  +   ?     k  	   p     z     �  /   �     �     �     �     �                    %     :     C     I  
   N     Y     e     l     q  \  v     �  '   �                    &     5  &   E  (   l     �     �     �     �  
   �     �     �     �     �  	             *  
   B     M     T     c     o     {     �     �     �     �     �     �     �     �  	   �     �     �     �               %     7  *   J     u     �     �  
   �     �     �     �     �     �  
   �     �     �     �  	   	       !   +     M     l     x     }     �     �     �     �     �     �     �     �     �                     2     J     S     j  
   |     �  	   �     �     �     �  	   �     �     �  %   �          )  (   ;  8   d     �  
   �     �  "   �  ?   �          7     J     N     T     `     h     z     �     �     �  
   �     �     �     �     �     U           f   F          t   5      +      6                  7      ?   4   %       -   !   &       [   N      9   B          k   A   V           s      (   @   a               d          	         $      E       L   ,   Z   Q   X   S         g           R   '   :              =       b       J   <       W              I   l       G              h       K   ;      
       1   ]      *      "   .   2   `       j   M   C   )      n       m   0   q               T   \         >         #       _       P       r   3       D   c   H   O   ^   i   Y   o   /   e   p      8                      (choose one) (filtered from _MAX_ total records) Actions Add Add New Add Plant Add User Add a new photo to the collection Add a new plant to the collection Are you sure to delete? Are you sure? Back Blog Botanics Cancel Close Comment Condition Credits Credits for people Credits for software Dashboard Delete Delete User Description Dimension Discussion Edit Edit User Family Feed Uses Gallery Habitat Help Help us History Home Id Industrial Uses Invalid Plant Invalid photo Invalid plant Invalid user Invalid username or password, try again Latest Comments Latest Plants Latitude Location Login Logout Longitude Map Medical Uses Meta-Data MimeType Name New Plant Next No plant attached Nothing found - sorry Or <a href="%s">register yourself</a> Password Photo Photo deleted Photo updated Photos Plant Plant created Plant deleted Plant updated Plants Please sign in Previous Processing... Profile Programmer Leader Project Leader Publish Publish your comment Reference URL Remember me Repeat password Respond Role Save Sayings and Lore Search... Search: Select Select Condition Select Subfamily Set Plant Showing 0 to 0 of 0 records Showing _START_ to _END_ of _TOTAL_ records Size Subfamily System The two passwords do not match. The user could not be saved. Please, try again. The user has been saved Traditional Uses URI Upload Upload Photos User User deleted User was not deleted Username Users View View Plant Your Plants delete edit view Project-Id-Version: fcadmin
POT-Creation-Date: 2013-05-22 00:20+0000
PO-Revision-Date: 2013-05-22 02:21+0100
Last-Translator: Javier <info@noknokstdio.com>
Language-Team: 
Language: Español
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.5
 (elige una) (filtrados de _MAX_ registros en total) Acciones Añadir Añadir nueva Añadir planta Añadir usuario Añadir una nueva foto a la colección Añadir una nueva planta a la colección ¿Seguro que desea elminar? ¿Esta seguro? Volver Blog Botánicos Cancelar Cerrar Comentar Estado de conservación Créditos Atribuciones a personas Atribuciones a software Escritorio Borrar Borrar usuario Descipción Dimensiones Conversación Editar Editar usuario Familia Usos de alimentación Galería Habitat Ayuda Ayudanos Historial Inicio Id Usos industriales Planta no válida Foto no válida Planta no válida Usuario no válido Nombre de usuario o contraseña no válido Últimos comentarios Últimas plantas Latitud Ubicación Identificarse Salir Longitud Mapa Usos medicinales Meta-datos MimeType Nombre Nueva planta Siguiente No hay planta vinculada No se encontro nada - lo sentimos O <a href="%s">regístrese</a> Contraseña Foto Foto borrada Foto actualizada Fotos Planta Planta creada Planta borrada Planta actualizada Plantas Por favor, identiquese Anterior Procesando... Perfil Director(a) de programación Director(a) de proyecto Publicar Publicar tu comentario URL de referencia Recordarme Repetir contraseña Responder Role Guardar Refranes y saber popular Buscar... Buscar: Seleccionar Selecciona un estado de conservación Selecciona una subfamilia Establecer planta Mostrando desde 0 hasta 0 de 0 registros Mostrando desde _START_ hasta _END_ de _TOTAL_ registros Tamaño Subfamilia Sistema Las dos contraseñas no coinciden. El usuario no pudo ser guardado. Por favor vuelva a intentarlo. El usuario ha sido guardado Usos tradicionales URI Subir Subir fotos Usuario Usuario eliminado El usuario no fué eliminado Nombre de usuario Usuarios Ver Ver planta Tus Plantas borrar editar ver 
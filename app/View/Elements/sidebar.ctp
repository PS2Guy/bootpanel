<div class="accordion" id="accordionSidebar" style="max-width: 350px;">
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
				<i class="icon-user"></i> <?php echo __('Account') ?>
			</a>
		</div>
		<div id="collapseOne" class="accordion-body collapse in">
			<div class="accordion-inner">
				<table class="table table-striped table-condensed">
					<tr>
						<td><b><?php echo __('User') ?></b></td>
						<td><?php echo $this->Session->read('Auth.User.username') ?></td>
					</tr>
					<tr>
						<td><b><?php echo __('Full Name') ?></b></td>
						<td><?php echo $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name') ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
				<i class="icon-cloud"></i> <?php echo __('Server') ?>
			</a>
		</div>
		<div id="collapseTwo" class="accordion-body collapse in">
			<div class="accordion-inner">
				<table class="table table-striped table-condensed">
					<tr>
						<td><b><?php echo __('Your IP') ?></b></td>
						<td><?php echo $this->Serverinfo->getClientIP() ?></td>
					</tr>
					<tr>
						<td><b><?php echo __('Server IP') ?></b></td>
						<td><?php echo $this->Serverinfo->getServerIP() ?></td>
					</tr>
					<tr>
						<td><b><?php echo __('Server OS') ?></b></td>
						<td><?php echo $this->Serverinfo->getServerOS(); ?></td>
					</tr>
					<tr>
						<td><b><?php echo __('Apache') ?></b></td>
						<td><?php echo $this->Serverinfo->getApacheVersion(); ?></td>
					</tr>

					<tr>
						<td><b><?php echo __('PHP') ?></b></td>
						<td><?php echo $this->Serverinfo->getPHPVersion(); ?></td>
					</tr>

					<tr>
						<td><b><?php echo __('MySQL') ?></b></td>
						<td><?php echo $this->Serverinfo->getMySQLVersion(); ?></td>
					</tr>

					<tr>
						<td><b><?php echo __('BootPanel') ?></b></td>
						<td><?php echo $this->Serverinfo->getBootPanelVersion(); ?></td>
					</tr>

					<tr>
						<td><b><?php echo __('Server Uptime') ?></b></td>
						<td><?php echo $this->Serverinfo->getServerUptime(); ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

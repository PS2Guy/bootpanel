<div class="navbar navbar-fixed-top navbar-inverse">
	<div class="navbar-inner">
		<div class="container-fluid">
			<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
 			
			<!-- Be sure to leave the brand out there if you want it shown -->
			<?php echo $this->Html->link($this->Html->image('branding/logo.png', array('style' => 'height:16px;')), array('plugin' => '', 'controller' => 'home', 'action' => 'index'), array('class' => 'brand', 'escape' => false, 'style' => 'padding: 8px 20px 8px;')) ?>
 
			<!-- Everything you want hidden at 940px or less, place within here -->
			<div class="nav-collapse collapse navbar-responsive-collapse">

				<!-- Main menu -->
				<ul class="nav">
					<li<?php if ($this->request['controller'] == 'home' && $this->request['action'] == 'index') echo ' class="active"'; ?>>
						<?php echo $this->Html->link(__('Home'), array('controller' => 'home', 'action' => 'index')) ?>
					</li>
					<li<?php if ($this->request['controller'] == 'hosts') echo ' class="active"'; ?>>
						<?php echo $this->Html->link(__('Hosts'), array('controller' => 'hosts', 'action' => 'index')) ?>
					</li>
					<li<?php if ($this->request['controller'] == 'domains') echo ' class="active"'; ?>>
						<?php echo $this->Html->link(__('Domains'), array('controller' => 'domains', 'action' => 'index')) ?>
					</li>
					<li<?php if ($this->request['controller'] == 'databases') echo ' class="active"'; ?>>
						<?php echo $this->Html->link(__('Databases'), array('controller' => 'databases', 'action' => 'index')) ?>
					</li>
				</ul>

				<?php if ($this->Session->read('Auth.User') != null) : ?>
					<ul class="nav pull-right">
						<!-- User menu -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<?php echo $this->Html->image('http://www.gravatar.com/avatar/'.md5($this->Session->read('Auth.User.email')).'?s=18', array('class' => 'img-circle')) ?>
								<?php echo $this->Session->read('Auth.User.username') ?>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><?php echo $this->Html->link(__('Profile'), array('controller' => 'users', 'action' => 'view', $this->Session->read('Auth.User.id'))) ?></li>
								<li class="divider"></li>
								
								<?php if ($this->Session->read('Auth.User.role') == 'admin') : ?>
									<!-- Administration menu -->
									<li class="nav-header"><?php echo __('Administration') ?></li>
									<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')) ?></li>
									<li class="divider"></li>
								<?php endif; ?>
								
								<li><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')) ?></li>
							</ul>
						</li>
					</ul>
				<?php else: ?>
					<ul class="nav pull-right">
						<li><?php echo $this->Html->link(__('Login'), array('controller' => 'users', 'action' => 'login')) ?></li>
					</ul>
				<?php endif; ?>

				<!-- Search form -->
				<div class="pull-right">
					<form class="navbar-form pull-left" action="">
						<input name="s" type="text" class="input-medium search-query" placeholder="<?php echo __('Search...') ?>" value="<?php if (isset($s)) echo $s ?>" x-webkit-speech />
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
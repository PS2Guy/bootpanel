<div id="footer" style="padding:20px;">
	<div class="row-fluid">
		<div class="span2">
			<h4><?php echo __('About RPGLive') ?></h4>
			<ul>
				<li><?php echo $this->Html->link(__('Credits'), '/pages/'.Configure::read('Config.language').'/credits') ?></li>
			</ul>
		</div>
		<div class="span2">
			<h4><?php echo __('Community') ?></h4>
			<ul>
				<li><?php echo $this->Html->link(__('Help us'), '/pages/'.Configure::read('Config.language').'/help_us') ?></li>
				<li><?php echo $this->Html->link(__('Blog'), 'http://fliwik.org/blog') ?></li>
			</ul>
		</div>
		<div class="span2">
			<h4><?php echo __('Help') ?></h4>
			<ul>
				<li><?php echo $this->Html->link(__('Primeros pasos'), '#') ?></li>
			</ul>
		</div>
		<div class="span2">
			<h4><?php echo __('Apps') ?></h4>
			<ul>
				<li><?php echo $this->Html->link(__('Android'), '#') ?></li>
				<li><?php echo $this->Html->link(__('iOS'), '#') ?></li>
			</ul>
		</div>
		<div class="span2">
			<h4><?php echo __('API') ?></h4>
			<ul>
				<li><?php echo $this->Html->link(__('REST API'), '#') ?></li>
			</ul>
		</div>
		<div class="span2">
			<h4><?php echo __('Social') ?></h4>
			<ul>
				<li><?php echo $this->Html->link(__('Twitter'), '#') ?></li>
				<li><?php echo $this->Html->link(__('Facebook'), '#') ?></li>
			</ul>
		</div>
	</div>
	<hr />
	<div class="row-fluid">
		<div class="span12">
			<?php echo $this->Html->link(__('Spanish'), '/users/change_language/es_ES') ?> <span style="color:#ddd;">|</span>
			<?php echo $this->Html->link(__('English'), '/users/change_language/en_US') ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_CO"><img alt="Licencia Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-sa/3.0/80x15.png" /></a> <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">RPGLive</span> se encuentra bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_CO">Licencia Creative Commons Atribución-NoComercial-CompartirIgual 3.0 Unported</a>.
		</div>
	</div>
</div>
<h2><?php echo __('Databases') ?></h2>

<?php echo $this->element('pagination') ?>

<table class="table">
	<tr>
		<th><?php echo __('Name') ?></th>
		<th><?php echo __('Type') ?></th>
		<th style="text-align: right;"><?php echo __('Actions') ?></th>
	</tr>
	<?php foreach ($databases as $database) : ?> 
		<tr>
			<td><?php echo $database['Database']['title'] ?></td>
			<td><?php echo $database['Database']['type'] ?></td>
			<td style="text-align: right;">
				<?php echo $this->Html->link(__('Edit'), '/databases/edit/'.$database['Database']['id']) ?>
				<?php echo $this->Html->link(__('Delete'), '/databases/edit/'.$database['Database']['id']) ?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>

<?php echo $this->element('pagination') ?>
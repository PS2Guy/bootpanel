<ul class="breadcrumb">
	<li><?php echo $this->Html->link(__('Home'), array('controller' => 'home', 'action' => 'index')) ?> <span class="divider">/</span></li>
	<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')) ?> <span class="divider">/</span></li>
	<li class="active"><?php echo __('Add') ?></li>
</ul>

<h2><?php echo __('Add User') ?></h2>

<?php echo $this->Form->create('User') ?>

<?php echo $this->Form->input('username', array('label' => __('Username'))) ?>
<br />
<?php echo $this->Form->input('password', array('label' => __('Password'))) ?>
<?php echo $this->Form->input('password_confirm', array('label' => __('Repeat password'), 'type' => 'password')) ?>
<br />
<?php echo $this->Form->input('role', array(
	'options' => array('admin' => 'Admin', 'author' => 'Author'),
	'label' => __('Role')
)) ?>

<div class="form-actions">
	<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary', 'div' => false)) ?>
	<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn')) ?>
</div>

<?php echo $this->Form->end(); ?>
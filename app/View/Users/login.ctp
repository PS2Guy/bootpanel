<?php echo $this->Form->create('User'); ?>
    
<h2><?php echo __('Please sign in'); ?></h2>

<?php echo $this->Form->input('username', array('label' => false, 'placeholder' => __('Username'))) ?>
<?php echo $this->Form->input('password', array('label' => false, 'placeholder' => __('Password'))) ?>
<?php echo $this->Form->input('remember-me', array('label' => array('text' => __('Remember me'), 'style' => 'display: inline-block;margin: 0 0 16px 8px;'), 'type' => 'checkbox', 'div' => false)) ?>
<?php echo $this->Form->submit(__('Login'), array('class' => 'btn btn-large btn-primary')); ?>
    
<?php echo $this->Form->end(); ?>

<h3><?php echo __('Or <a href="%s">register yourself</a>', $this->Html->url('/users/register')) ?></h3>
<h2><?php echo __('Users') ?></h2>

<?php echo $this->element('pagination') ?>
<table class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th>#</th>
			<th><?php echo __('Username') ?></th>
			<th><?php echo __('Role') ?></th>
			<th style="text-align: right"><?php echo __('Actions') ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user) : ?>
		<tr>
			<td><?php echo $user['User']['id'] ?></td>
			<td><?php echo $user['User']['username'] ?></td>
			<td><?php echo $user['User']['role'] ?></td>
			<td style="text-align: right">
				<?php echo $this->Html->link('<i class="icon-eye-open"></i>', array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-small', 'escape' => false, 'alt' => __('view'))) ?>
				<?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-small', 'escape' => false, 'alt' => __('edit'))) ?>
				<?php echo $this->Html->link('<i class="icon-remove"></i>', '#deleteUser'.$user['User']['id'], array('class' => 'btn btn-small btn-danger', 'escape' => false, 'alt' => __('delete'), 'data-toggle' => 'modal')) ?>
			</td>
			<!-- Modal -->
			<div id="deleteUser<?php echo $user['User']['id'] ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel"><?php echo __('Delete User') ?></h3>
				</div>
				<div class="modal-body">
					<p><?php echo __('Are you sure?') ?></p>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
					<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger')) ?>
				</div>
			</div>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $this->element('pagination') ?>

<?php echo $this->Html->link(__('Add User'), array('action' => 'add'), array('class' => 'btn')) ?>
<h2><?php echo __('Hosts') ?></h2>

<?php echo $this->element('pagination') ?>

<table class="table">
	<tr>
		<th><?php echo __('Name') ?></th>
		<th><?php echo __('Type') ?></th>
		<th style="text-align: right;"><?php echo __('Actions') ?></th>
	</tr>
	<?php foreach ($hosts as $host) : ?> 
		<tr>
			<td><?php echo $host['Host']['title'] ?></td>
			<td><?php echo $this->element('hosts/type', array('type' => $host['Host']['type'])) ?></td>
			<td style="text-align: right;">
				<?php echo $this->Html->link(__('Edit'), '/hosts/edit/'.$host['Host']['id']) ?>
				<?php echo $this->Html->link(__('Delete'), '/hosts/edit/'.$host['Host']['id']) ?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>

<?php echo $this->element('pagination') ?>
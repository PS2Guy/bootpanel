<?php
App::uses('String', 'Utility');
App::uses('HtmlHelper', 'View/Helper');
class ServerinfoHelper extends HtmlHelper {


	public function getClientIP() {
		return $this->_View->request->clientIp();
	}

	public function getServerIP() {
		return $this->_View->request->host();
	}

	public function getServerOS() {
		return php_uname();
	}

	public function getApacheVersion() {
		return apache_get_version();
	}

	public function getPHPVersion() {
		return phpversion();
	}

	public function getMySQLVersion() {
		return mysql_get_server_info();
	}

	public function getBootPanelVersion() {
		return Configure::read('Version');
	}

	public function getServerUptime() {

		if (strpos(strtolower(PHP_OS),'win') !== FALSE) {

			// For Windows
			$file = 'c:\pagefile.sys';
			$uptime = (time() - filemtime($this->file));

			$days = floor($uptime / (24 * 3600));
			$uptime = $uptime - ($days * (24 * 3600));
			$hours = floor($uptime / (3600));
			$uptime = $uptime - ($hours * (3600));
			$minutes = floor($uptime / (60));
			$uptime = $uptime - ($minutes * 60);
			$seconds = $uptime;
			
			$days = $days.' day'.($days != 1 ? 's' : '');
			$hours = $hours.' hour'.($hours != 1 ? 's' : '');
			$minutes = $minutes.' minute'.($minutes != 1 ? 's' : '');
			$seconds = $seconds.' second'.($seconds != 1 ? 's' : '');

			$uptime = $days.' '.$hours.' '.$minutes.' '.$seconds;

    	} else {

    		// For *nix
    		if (in_array('exec',$disabled_funcs)) {
		    	$load = file_get_contents("/proc/loadavg");
		      	$load = explode(' ',$load);
		      	$loadnow = $load[0];
		      	$load15 = $load[1];
		      	$load30 = $load[2];
    		} else if (($reguptime = trim(exec("uptime"))) && preg_match("/, *(\\d) (users?), .*: (.*), (.*), (.*)/",$reguptime,$uptime)) {
      			$users[0] = $uptime[1];
      			$users[1] = $uptime[2];
      			$loadnow = $uptime[3];
      			$load15 = $uptime[4];
      			$load30 = $uptime[5];
    		}
    		if (in_array('shell_exec',$disabled_funcs)) {
      			$uptime_text = file_get_contents("/proc/uptime");
      			$uptime = substr($uptime_text,0,strpos($uptime_text," "));
    		} else {
      			$uptime = shell_exec("cut -d. -f1 /proc/uptime");
    		}
    		$days = floor($uptime/60/60/24);
    		$hours = str_pad($uptime/60/60%24,2,"0",STR_PAD_LEFT);
    		$mins = str_pad($uptime/60%60,2,"0",STR_PAD_LEFT);
    		$secs = str_pad($uptime%60,2,"0",STR_PAD_LEFT);
    		$uptime_str = "$days Days $hours:$mins:$secs";

    	}

    	return $uptime;
		//$server = phpinfo(INFO_GENERAL);
		//debug($server);
		//return $server;
	}

	

}
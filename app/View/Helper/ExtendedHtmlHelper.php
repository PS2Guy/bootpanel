<?php
App::uses('String', 'Utility');
App::uses('HtmlHelper', 'View/Helper');
class ExtendedHtmlHelper extends HtmlHelper {

	public $accordionUID;

	public function accordionStart() {
		$this->accordionUID = String::uuid();
		return '<div class="accordion" id="accordion-'.$this->accordionUID.'">';
	}

	public function accordionBlock($title = '', $content = '') {
		$uid = String::uuid();

		$out = '<div class="accordion-group">';
		$out .= '<div class="accordion-heading"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-'.$this->accordionUID.'" href="#collapse-'.$uid.'">'.$title.'</a></div>';
		$out .= '<div id="collapse-'.$uid.'" class="accordion-body collapse"><div class="accordion-inner">'.$content.'</div></div>';
		$out .= '</div>';
		return $out;
	}

	public function accordionEnd() {
		return '</div>';
	}

}
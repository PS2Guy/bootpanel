<?php
App::uses('FormHelper', 'View/Helper');
class ExtendedFormHelper extends FormHelper {

	public $helpers = array('Html');

	public function tiny($fieldName, $options = array()){
		$options['type'] = 'textarea';
        $out = parent::input($fieldName, $options);

        $options = $this->_initInputField($fieldName, $options);

        $out .= $this->Html->scriptBlock("tinymce.init({
        	selector: 'textarea#".$options['id']."',
        	plugins: [
        		'advlist autolink lists link image anchor',
        		'visualblocks fullscreen',
        		'insertdatetime media contextmenu paste'
        	],
        	toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        	menubar:false, 
    		statusbar: false
    	});");
        return $out;
    }

    public function select2($fieldName, $options = array()) {
    	$options['type'] = 'select';
        $out = parent::input($fieldName, $options);

        $options = $this->_initInputField($fieldName, $options);
        if (!isset($options['placeholder'])) $options['placeholder'] = '';

        $out .= $this->Html->scriptBlock("$(function() {
        	$('#".$options['id']."').select2({
        		placeholder: '".$options['placeholder']."',
        		allowClear: true
        	});
    	});");

        return $out;
    }

}
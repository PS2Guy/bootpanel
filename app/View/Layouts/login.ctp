<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<?php echo $this->Html->css('bootstrap/bootstrap') ?>
	<?php echo $this->Html->css('bootstrap/bootstrap-responsive.min') ?>
	<?php echo $this->Html->css('default') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php echo $this->fetch('meta') ?>
	<?php echo $this->fetch('css') ?>
</head>
<body>
	<div id="content" style="max-width: 300px;margin: 0 auto 20px;">
		<?php echo $this->Session->flash('flash', array('element' => 'messages/error')); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	
	<?php echo $this->Html->script('jquery-2.0.1.min') ?>
	<?php echo $this->Html->script('bootstrap/bootstrap.js') ?>
	<?php echo $this->fetch('script') ?>
</body>
</html>

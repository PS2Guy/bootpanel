<!DOCTYPE html>
<html lang="es">
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	
	<meta http-equiv="x-ua-compatible" content="IE=10">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php echo $this->fetch('meta') ?>

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->Html->url('/img/branding/favicon.png') ?>" />
	<link rel="apple-touch-icon" href="<?php echo $this->Html->url('/img/branding/favicon_iphone.png') ?>" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Html->url('/img/branding/favicon_ipad.png') ?>" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Html->url('/img/branding/favicon_ipad.png') ?>" />

	<?php echo $this->Html->css(array('bootstrap/bootstrap.min', 'bootstrap/bootstrap-fliwik')) ?>
	<?php echo $this->Html->css(array('bootstrap/bootstrap-responsive.min', 'bootstrap/bootstrap-fliwik-responsive')) ?>
	<?php echo $this->Html->css(array('font-awesome/font-awesome.min')) ?>
	<?php echo $this->Html->css('select2/select2') ?>
	<?php echo $this->Html->css('default') ?>
	<?php echo $this->fetch('css') ?>

	<?php echo $this->Html->script('jquery-2.0.1.min') ?>
	<?php echo $this->Html->script('bootstrap/bootstrap.min') ?>
	<?php echo $this->Html->script('tinymce/tinymce.min') ?>
	<?php echo $this->Html->script('underscore-min') ?>
	<?php echo $this->Html->script('select2/select2.min') ?>
	<?php echo $this->Html->script('raty/jquery.raty.min') ?>
	<?php echo $this->fetch('script') ?>
</head>
<body>
	<div id="wrap">
		<?php echo $this->element('menu') ?>
		<!-- Begin page content -->

		<div class="container container-fluid">
  			<div class="row-fluid">
    			<div class="span4">
      				<aside>
      					<?php echo $this->element('sidebar') ?>
      				</aside>
    			</div>
    			<div class="span8">
      				<?php echo $this->Session->flash(); ?>
						<?php echo $this->Html->getCrumbList(array('class' => 'breadcrumb', 'separator' => '<span class="divider">/</span>', 'firstClass' => false, 'lastClass' => 'active')) ?>
						<?php echo $this->fetch('content'); ?>
						<div style="height: 5000px;"></div>
    			</div>
  			</div>
		</div>
		<div id="push"></div>
		
	</div>
	<?php echo $this->element('footer') ?>
</body>
</html>

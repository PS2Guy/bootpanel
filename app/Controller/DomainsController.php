<?php
App::uses('AppController', 'Controller');
class DomainsController extends AppController {

	public $name = 'Domains';
	
	public function index() {
		$this->set('title_for_layout', __('Domains'));
	}

}

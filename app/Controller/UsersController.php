<?php
App::uses('AppController', 'Controller');
class UsersController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
		$this->Auth->allow('view');
    }
	
	public function login() {
        $this->layout = 'login';
		if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
	            $this->redirect($this->Auth->redirect());
	        } else {
	            $this->Session->setFlash(__('Invalid username or password, try again'), 'messages/error');
	        }
	    }
	}
	
	public function logout() {
	    $this->redirect($this->Auth->logout());
	}

    public function index() {
        if ($this->Session->read('Auth.User.role') != 'admin') $this->redirect($this->Auth->redirect());
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
        $this->set('title_for_layout', __('Users'));
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) throw new NotFoundException(__('Invalid user'));
        $this->set('user', $this->User->read(null, $id));
        $this->set('title_for_layout', __('User'));
    }

    public function add() {
        if ($this->request->is('post')) {
			if ($this->data['User']['password'] != $this->data['User']['password_confirm']) {
				$this->Session->setFlash(__('The two passwords do not match.'), 'messages/error');
			} else {
				$this->User->create();
				$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
	            if ($this->User->save($this->request->data)) {
	                $this->Session->setFlash(__('The user has been saved'), 'messages/success');
	                $this->redirect(array('action' => 'index'));
	            } else {
	                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'messages/error');
	            }
			}
        }
        $this->set('title_for_layout', __('User'));
    }

    public function edit($id = null) {
        if ($this->Session->read('Auth.User.role') != 'admin' && $this->Session->read('Auth.User.id') != $id) $this->redirect($this->Auth->redirect());
        $this->User->id = $id;
        if (!$this->User->exists()) throw new NotFoundException(__('Invalid user'));
		
        if ($this->request->is('post') || $this->request->is('put')) {
        	
			if (!empty($this->data['User']['password_confirm'])) {
				if ($this->data['User']['password'] != $this->data['User']['password_confirm']) {
					$this->Session->setFlash(__('The two passwords do not match.'), 'messages/error');
					$this->redirect(array('action' => 'index'));
				} else {
					$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
				}
			} else {
				unset($this->request->data['User']['password']);
			} 	
			
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'), 'messages/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'messages/error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
        $this->set('title_for_layout', __('User'));
    }

    public function delete($id = null) {
        if ($this->Session->read('Auth.User.role') != 'admin') $this->redirect($this->Auth->redirect());
        if (!$this->request->is('post')) throw new MethodNotAllowedException();
        $this->User->id = $id;
        if (!$this->User->exists()) throw new NotFoundException(__('Invalid user'));
		
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User deleted'), 'messages/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'), 'messages/error');
        $this->redirect(array('action' => 'index'));
    }
}
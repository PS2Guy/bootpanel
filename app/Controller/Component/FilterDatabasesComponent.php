<?php
App::uses('Component', 'Controller');
class FilterDatabasesComponent extends Component {
	
    public $components = array('Session');

	public function get() {
        $this->controller = $this->_Collection->getController();
        $this->controller->paginate = array(
            'conditions' => $this->getConditions(),
            'joins' => $this->getJoins(),
            'order' => $this->getOrder(),
            'limit' => $this->getLimit(),
            'maxLimit' => 100
        );
		return $this->controller->paginate('Database');
	}
	
    public function getConditions() {
    	$this->controller = $this->_Collection->getController();
		$conditions = array();

        // Search: s
        if (array_key_exists('s', $this->controller->request->params['named'])) {
            $conditions += array("OR" => array('Databases.name LIKE' => "%".urldecode($this->controller->request->params['named']['s'])."%"));
            $this->controller->set('s', $this->controller->request->params['named']['s']);
        }
        
        // Filter Owner: own
        if ($this->Session->read('Auth.User') != null) {
            if (array_key_exists('own', $this->controller->request->params['named']) && $this->controller->request->params['named']['own'] != 'false') {
                $conditions += array('Databases.user_id' => $this->Session->read('Auth.User.id'));
                $this->controller->set('own', $this->controller->request->params['named']['own']);
            }
        }

		return $conditions;
    }

    public function getLimit() {
        $this->controller = $this->_Collection->getController();
        $limit = 100;
        if (array_key_exists('limit', $this->controller->request->params['named'])) $limit = $this->controller->request->params['named']['limit'];
        $this->controller->set('limit', $limit);
        return $limit;
    }
    
    public function getOrder() {
        $this->controller = $this->_Collection->getController();
    	$order = array();
        if (array_key_exists('ord', $this->controller->request->params['named'])) {
            switch($this->controller->request->params['named']['ord']) {
                case 'latest':
                case '': $order += array('Databases.created' => 'DESC'); break;
            }
            $this->controller->set('ord', $this->controller->request->params['named']['ord']);
        }
        return $order;
    }

    public function getJoins() {
        $this->controller = $this->_Collection->getController();
        $joins = array();
		return $joins;
    }
    
}
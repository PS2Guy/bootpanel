<?php
App::uses('AppController', 'Controller');
class DatabasesController extends AppController {

	public $name = 'Databases';
	public $components = array('FilterDatabases');
	
	public function index() {
		$databases = $this->FilterDatabases->get();
		$this->set(compact('databases'));
		$this->set('title_for_layout', __('Databases'));
	}
	
}

<?php
App::uses('AppController', 'Controller');
class HostsController extends AppController {

	public $name = 'Hosts';
	public $components = array('FilterHosts');
	
	public function index() {
		$hosts = $this->FilterHosts->get();
		$this->set(compact('hosts'));
		$this->set('title_for_layout', __('Hosts'));
	}
	
}

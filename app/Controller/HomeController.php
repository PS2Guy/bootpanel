<?php
App::uses('AppController', 'Controller');
class HomeController extends AppController {

	public $name = 'Home';
	public $uses = array();
	
	public function beforeFilter() {
        parent::beforeFilter();
		//$this->Auth->allow('index', 'map', 'demo');
    }
	
	public function index() {
		$this->set('title_for_layout', __('Home'));
	}
	
	public function map() {
		$this->set('title_for_layout', __('Map'));
	}

	public function demo() {
		$this->set('title_for_layout', __('Demo'));
	}
}

BootPanel
=========

BootPanel is a CakePHP based app for server administration.
I'am based on ZPanel work and searching for a better work distribution.
Using Bootstrap (Jasny variation) from base for multiple device support.

Some Handy Links
----------------

[CakePHP](http://www.cakephp.org) - The rapid development PHP framework

[ZPanel](http://www.zpanelcp.com) - ZPanel is a free and complete web hosting control panel for Microsoft® Windows™ and POSIX (Linux, UNIX and MacOSX) based servers

[Bootstrap](http://twitter.github.io/bootstrap) - Bootstrap. Sleek, intuitive, and powerful front-end framework for faster and easier web development

[Jasny Bootstrap](http://jasny.github.io/bootstrap) - Bootstrap improved.
